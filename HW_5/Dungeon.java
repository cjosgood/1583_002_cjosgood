public class Dungeon {
	
	//instance variables
	private static Room balcony;
	private static Room bedroom1;
	private static Room bedroom2;
	private static Room dining;
	private static Room kitchen;
	private static Room northHall;
	private static Room southHall;
	private static Room currentRoom;
	
	//constructor
	public Dungeon(Room balcony, Room bedroom1, Room bedroom2, Room dining, Room kitchen, Room northHall, Room southHall, Room currentRoom) {
		this.balcony = balcony;
		this.bedroom1 = bedroom1;
		this.bedroom2 = bedroom2;
		this.dining = dining;
		this.kitchen = kitchen;
		this.northHall = northHall;
		this.southHall = southHall;
		this.currentRoom = currentRoom;
	}
	public static void establishRooms() {
		Room bedroom2 = new Room("You are in the Guest Bedroom", bedroom1, null, southHall, null);
		Room southHall = new Room("You are in South Hall", northHall, null, dining, bedroom2);
		Room dining = new Room("You are in the Dining Room", kitchen, null, null, southHall);
		Room bedroom1 = new Room("You are in the Master Bedroom", null, bedroom2, northHall, null);
		Room northHall = new Room("You are in the North Hall", balcony, southHall, kitchen, bedroom1);
		Room kitchen = new Room("You are in the Kitchen", null, dining, null, northHall);
		Room balcony = new Room("You are on the Balcony", null, southHall, null, null);
	}
	
	//getter methods
	public static Room getFirstRoom() {
		return currentRoom;
	}
} //end class
	