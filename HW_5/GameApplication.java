import java.util.Scanner;
import java.util.Random;

public class GameApplication {
	
	private static  Room currentRoom; //starting room
	private static boolean gameOver = false; //loop-control variable
	private static char playerDirectionChoice;
	
	private static Dungeon gameDungeon = new Dungeon();
	static Scanner getUserInput = new Scanner(System.in); //user input
	
	public static void main(String[] args) {
		gameDungeon.establishRooms(); 
		currentRoom = gameDungeon.getFirstRoom();
		
		while(gameOver = false) {
			
			playerDirectionChoice = getUserInput.nextLine().charAt(0);
			
			if(playerDirectionChoice == 'n' || playerDirectionChoice == 's' || playerDirectionChoice == 'e' || playerDirectionChoice == 'w' || playerDirectionChoice == 'q') {
				moveToNextRoom();
			}
		}
	}//end of main method
	
	private static void moveToNextRoom() {
		boolean notAnExit = false;
		if (playerDirectionChoice == 'n' && currentRoom.getRoom("north") != null) {
			currentRoom = currentRoom.getNorth("north");
			notAnExit = true;
		}
		else if (!notAnExit) {
			System.out.print("This is not an exit.");
		}
		if (playerDirectionChoice == 's' && currentRoom.getRoom("south") != null) {
			currentRoom = currentRoom.getSouth("south");
			notAnExit = true;
		}
		else if (!notAnExit) {
			System.out.print("This is not an exit.");
		}
		if (playerDirectionChoice == 'e' && currentRoom.getRoom("east") != null) {
			currentRoom = currentRoom.getEast("east");
			notAnExit = true;
		}
		else if (!notAnExit) {
			System.out.print("This is not an exit.");
		}
		if (playerDirectionChoice == 'w' && currentRoom.getRoom("west") != null) {
			currentRoom = currentRoom.getWest("west");
			notAnExit = true;
		}
		else if (!notAnExit) {
			System.out.print("This is not an exit.");
		}
		if (playerDirectionChoice == 'q') { //quit the game
			gameOver = true;
			System.out.println("You quit the game.");
		}
	}
}
