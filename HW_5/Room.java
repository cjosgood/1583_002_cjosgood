public class Room {
	
	//instance variables
	private String description;
	private Room north;
	private Room south;
	private Room east;
	private Room west;
	private String exits;
	
	//constructor-we call this to build an object, and in it, we should initialize to each of our instance variables
	public Room(String description, Room north, Room south, Room east, Room west) {
		this.description = description;
		this.north = north;
		this.south = south;
		this.east = east;
		this.west = west;
	}
	
	//setter methods
	public void setNorth(Room north) {
		this.north = north;
	}
	
	public void setSouth(Room south) {
		this.south = south;
	}
	
	public void setEast(Room east) {
		this.east = east;
	}
	
	public void setWest(Room west) {
		this.west = west;
	}
	
	public void setExits(Room north, Room south, Room east, Room west) {
		this.exits = exits;
	}
	
	
	//getter methods
	public Room getNorth() {
		return this.north;
	}
	
	public Room getSouth() {
		return this.south;
	}
	
	public Room getEast() {
		return this.east;
	}
	
	public Room getWest() {
		return this.west;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getExits() {
		return this.exits;
	}
	
	public String toString() {
		return description + north + south + east + west + exits;
	}
}//end class
	
	
	
		
		