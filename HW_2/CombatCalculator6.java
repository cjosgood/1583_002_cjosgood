import java.util.Scanner;
public class CombatCalculator6
{
		public static void main(String[] args)
		{
			Scanner input = new Scanner(System.in);
		/*Monster data variables*/
		//Declare variable for monster's name and initialize it to "goblin"
			String name = "goblin";
		//Declare variable for monster's health and initialize it 100
			int health1 = 100;
		//Declare variable for monster's attack power and initialize it to 15
			int attackPower1 = 15;
		
		/*Hero data variables*/
		//Declare variable for Hero's health and initialize it to 100
			int health2 = 100;
		//Declare variable for Hero's attack power and initialize it to 12
			int attackPower2 = 12;
		//Declare variable for Hero's magic power and initialize it to 0
			int magicPower = 0;
			
		/*Loop Control*/ 
		health1 = 100;
		while (health1 >= 0) {
		
			/*Report Combat Stats*/
			//Print the Monster's name
				System.out.println( "You are fighting a goblin!");
			//Print the Monster's health
				System.out.println( "The Monster HP:" + health1 );
			//Print the Player's health
				System.out.println( "Your HP: 100" );
			//Print the Player's magic points
				System.out.println( "Your MP:" + magicPower );
			
			/*Combat menu prompt*/
				System.out.println( "\nCombat Options: ");
			//Print option 1: Sword Attack
				System.out.println( " 1.) Sword Attack");
			//Print option 2: Cast Spell
				System.out.println( " 2.) Cast Spell");
			//Print option 3: Charge Mana
				System.out.println( " 3.) Charge Mana");
			//Print option 4: Run Away
				System.out.println( " 4.) Run Away");
			//Prompt player for action
				System.out.printf( "What action do you want to perform? ");
			int number = input.nextInt();
			
		//Declare variable for user input (as number) and acquire value from Scanner object
	
			if (number == 1) {
				health1 = health1 - 12;
				System.out.println("\nYou strike the goblin with your sword for 12 damage.\n"); 
			}
			else if (number == 2) {
				health1 = health1 / 2;
				System.out.println("\nYou cast the weaken spell on the monster.\n"); 
			}
			else if (number == 3) {
				magicPower = magicPower + 1;
				System.out.println("\nYou focus and charge your magic power.\n");
			}
			else if (number == 4) { 
				System.out.println("\nYou run away!\n");
				break;
			}
			else {
				System.out.println("\nI don't understand that command.\n");
			}
			if (health1 <= 0) {
				System.out.println("\nYou defeated the goblin!\n");
			}
		
		}
		
	}
}