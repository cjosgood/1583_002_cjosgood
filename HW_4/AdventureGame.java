import java.util.Scanner;
public class AdventureGame 
{
		private static int NUMBER_OF_ROOMS = 7;
		private static int currentRoom = 0;
		private static int north = 0;
		private static int east = 1;
		private static int west = 2;
		private static int south = 3;
		
		public static void main(String [] args) 
		{
			String[] roomDescriptionArray = new String[7];
			roomDescriptionArray[0] = "You are in the Guest Bedroom, exits are North and East.";
			roomDescriptionArray[1] = "You are in the South Hall, exits are North, East, and West.";
			roomDescriptionArray[2] = "You are in the Dining Room, exits are North and West.";
			roomDescriptionArray[3] = "You are in the Master Bedroom, exits are East and South.";
			roomDescriptionArray[4] = "You are in the North Hall, exits are North, East, West, and South.";
			roomDescriptionArray[5] = "You are in the Kitchen, exits are West and South.";
			roomDescriptionArray[6] = "You are on the Balcony, exits are South.";
			
			int[][] roomExit = {{3, 1, -1, -1}, {4, 2, 0, -1}, {5, -1, 1, -1}, {-1, 4, -1, 0}, {6, 5, 3, 1}, {-1, -1, 4, 2}, {-1, -1, -1, 4}};
			
			for (int i = 7; 1>0; i++)
			{
				System.out.println(roomDescriptionArray[currentRoom]);
				System.out.println("Which direction would you like to go? Type N, S, W, or E. To quit the game, type Q.");
				Scanner input = new Scanner(System.in);
				char response = input.nextLine().charAt(0);
				System.out.println(" ");
				
				if (response == 'n' || response == 'N')
				{
					if ( roomExit [currentRoom][north] < 0)
					{
						System.out.println("There is no exit that way.\n");
					}
					else
					{
						currentRoom = roomExit[currentRoom][north];
					}
				}
				else if (response == 'e' || response == 'E')
				{
					if ( roomExit [currentRoom][east] < 0)
					{
						System.out.println("There is no exit that way.\n");
					}
					else
					{
						currentRoom = roomExit[currentRoom][east];
					}
				}
				else if (response == 'w' || response == 'W')
				{
					if ( roomExit [currentRoom][west] < 0)
					{
						System.out.println("There is no exit that way.\n");
					}
					else
					{
						currentRoom = roomExit[currentRoom][west];
					}
				}
				else if (response == 's' || response == 's')
				{
					if ( roomExit [currentRoom][south] < 0)
					{
						System.out.println("There is no exit that way.\n");
					}
					else
					{
						currentRoom = roomExit[currentRoom][south];
					}
				}
				else if (response == 'q' || response == 'Q')
				{
						System.out.println("You left the castle.");
						break;
				}	
			}
		}
	}			
	
	
	
		
		
	